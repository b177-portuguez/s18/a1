
function Trainer(name, age, pokemon, friends){
	// Properties
	this.name = name;
	this.age = age;
	this.pokemon = pokemon;
	this.friends = friends;
	// Methods
	this.talk = function(selectedPokemon){
		console.log(selectedPokemon + "! I choose you!");
	}
}

let caleb = new Trainer(
		"Caleb Alexander", 
		12, 
		['Gengar','Marowak','Dreepy','Marshadow'], 
		{
			Stana: ['Ryan','Isha'],
			Kingstown: ['Gabe','Aimee'],
		}
	)
console.log(caleb);


console.log(caleb.name);


console.log(caleb['pokemon']);


console.log(caleb.talk('Gengar'));


function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	// Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		newhealth = (target.health - this.attack);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		if (newhealth <= 0) {
			target.faint();
		}
	}
	this.faint = function(){
		console.log(this.name + ' fainted');
	}
}


let gengar = new Pokemon("Gengar", 17);
let marowak = new Pokemon("Marowak", 28);
let dreepy = new Pokemon("Dreepy", 12);
let marshadow = new Pokemon("Marshadow", 11);
let ghastly = new Pokemon("Ghastly", 11);
let rotom = new Pokemon("Rotom", 5);

console.log(gengar);
console.log(rotom);
console.log(ghastly);

gengar.tackle(rotom);
gengar.tackle(marowak);
